import {Component, Host, h, Prop} from '@stencil/core';

@Component({
  tag: 'request-url',
  styleUrl: 'request-url.scss',
  shadow: true,
})
export class RequestUrl {
  @Prop() url: string;

  render() {
    return (
      <Host>
        <div class={"request-url"}>{this.url}</div>
      </Host>
    );
  }

}
