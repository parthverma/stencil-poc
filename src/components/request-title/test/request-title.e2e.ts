import { newE2EPage } from '@stencil/core/testing';

describe('request-title', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<request-title></request-title>');

    const element = await page.find('request-title');
    expect(element).toHaveClass('hydrated');
  });
});
