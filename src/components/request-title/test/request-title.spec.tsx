import { newSpecPage } from '@stencil/core/testing';
import { RequestTitle } from '../request-title';

describe('request-title', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [RequestTitle],
      html: `<request-title></request-title>`,
    });
    expect(page.root).toEqualHtml(`
      <request-title>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </request-title>
    `);
  });
});
