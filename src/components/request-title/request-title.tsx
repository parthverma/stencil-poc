import {Component, Host, h, Prop,getAssetPath} from '@stencil/core';
// import {Icon} from '@postman/aether';
@Component({
  tag: 'request-title',
  styleUrl: 'request-title.scss',
  shadow: true,
  assetsDirs: ['assets']
})
export class RequestTitle {
  @Prop() method: string;
  @Prop() name: string;
  @Prop() isAuthenticated: boolean;

  render() {
    return (
      <Host>
        <span class={`request-method ${this.method.toLowerCase()}`}>{this.method}</span>
        <p class={`request-title`}>{this.name}</p>
        {this.isAuthenticated && <img src={getAssetPath('./assets/ic-auth@3x.png')} height={13} width={10}/> }
      </Host>
    );
  }

}
